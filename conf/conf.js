var HtmlReporter = require('protractor-html-screenshot-reporter');
var JasmineReporter = require('jasmine-reporters');

exports.config = {
  framework: 'jasmine2',

  seleniumAddress: 'http://localhost:4444/wd/hub',

  specs: ['../test/specs/*Spec.js'],

  /*
    multiCapabilities: [{
        browserName: 'firefox'
      }, {
        browserName: 'chrome'
    }],
  */
  capabilities: {
      'browserName': 'chrome',
      'chromeOptions': {
        'args': ['no-sandbox','no-default-browser-check','no-first-run','disable-default-apps']
      }
  },

  allScriptsTimeout : 300000, // 5*60*1000 = 5 mins

  onPrepare: function() {
    // At this point, global 'protractor' object will be set up, and jasmine
    // will be available. For example, you can add a Jasmine reporter with:

	// set implicit wait times in ms...
    browser.manage().timeouts().implicitlyWait(5000);

    browser.driver.manage().window().maximize();
    //browser.driver.manage().window().setSzie(768, 1000);
    //browser.driver.manage().window().setSzie(400, 600);
    //global.browser.ignoreSynchronization = true;

    jasmine.getEnv().addReporter(new JasmineReporter.JUnitXmlReporter({
		consolidateAll : true,
		filePrefix : 'xmloutput',
		savePath: './Results'
	}));

    jasmine.getEnv().addReporter(new HtmlReporter({
		baseDirectory: '/tmp/screenshots'
	}));
  },
    // ----- Options to be passed to minijasminenode -----
    jasmineNodeOpts: {
      // onComplete will be called just before the driver quits.
      onComplete: null,
      // If true, display spec names.
      isVerbose: true,
      // If true, print colors to the terminal.
      showColors: true,
      // If true, include stack traces in failures.
      includeStackTrace: true,
      // Default time to wait in ms before a test fails.
      defaultTimeoutInterval: 60000
  }
};